+++
categories = ["news"]
date = "2018-03-30T12:51:50+07:00"
description = "Grab, platform penyedia layanan transportasi on-demand dan pembayaran mobile terdepan di Asia Tenggara, hari ini mengumumkan kerja sama strategis dengan PayTren"
featured = ["featured"]
thumbnail = "/images/paytren-grab.png"
slug = ""
tags = ["grab", "paytren"]
title = "PayTren dan Grab Jalin Kerja Sama Strategis"

+++
Grab, platform penyedia layanan transportasi _on-demand_ dan pembayaran _mobile_ terdepan di Asia Tenggara, hari ini mengumumkan kerja sama strategis dengan PayTren, salah satu aplikasi pembayaran dan transaksi _mobile_ terkemuka di Indonesia. 

Penandatanganan perjanjian kerja sama antara kedua belah pihak akan memungkinkan PayTren dan Grab untuk mengintegrasikan kekuatan mitra dan mitra pengemudi yang dimilikinya untuk membentuk jaringan pengusaha mikro terbesar di Indonesia dengan lebih dari tiga juta pengusaha mikro yang akan memperoleh penghasilan tambahan dan membagikan kesempatan kerja sebagai mitra pengemudi Grab kepada masyarakat di sekitarnya.

Kerja sama strategis antara Grab dan PayTren akan meningkatkan kapasitas para pengusaha mikro untuk menggerakkan usaha-usaha baru, yang pada gilirannya akan mendorong pembangunan ekonomi yang berbasis partisipasi masyarakat luas sebagai pelaku usaha dan percepatan persebaran lapangan usaha digital di Tanah Air sebagai dampak dari kemajuan ekonomi digital.

Mulai pertengahan Januari 2018 mendatang, para mitra PayTren di seluruh Indonesia dapat merekrut mitra pengemudi Grab melalui aplikasi PayTren dan memperoleh pendapatan tambahan. 

Ke depannya, melalui teknologi dan produk yang disediakan oleh Kudo yang dimiliki Grab, mitra PayTren akan semakin memiliki beragam potensi usaha menarik yang dapat membangun semangat jiwa kewirausahaan sekaligus peningkatan taraf kesejahteraan mereka.

Ekonomi Digital Indonesia terus berkembang pesat dengan tingginya tingkat adopsi internet, namun karena rendahnya tingkat adopsi rekening bank, banyak kalangan menengah di Indonesia yang belum dapat menikmati manfaat dari ekonomi digital. 

Model bisnis yang dimiliki oleh PayTren dan platform teknologi Kudo yang dimiliki Grab telah terbukti sebagai solusi lokal yang sukses mengatasi tantangan tersebut. Para mitra PayTren tersebut akan membantu masyarakat Indonesia yang memiliki pengalaman terbatas atau bahkan yang tidak memiliki pengalaman digital sekalipun untuk mengakses layanan _online_. 

Sebaliknya, sebagai mitra mereka juga memiliki kesempatan untuk memperoleh penghasilan tambahan, dimana kini agen-agen Kudo telah berhasil memperoleh pendapatan tambahan sebesar 30 persen per bulan.

Salah satu contoh dari agen yang telah berhasil meraih kesuksesan dengan bisnis model tersebut adalah Ibu Ai Sumiati, agen Kudo di Jakarta. “Saya memiliki toko akuarium dan ikan hias. 

Bisnis di toko kami mengalami pasang surut dan kami sangat bersyukur bahwa kini memiliki lebih banyak pilihan untuk memperoleh penghasilan. 

Saya menggunakan platform teknologi Grab-Kudo setiap saat untuk menjual pulsa telepon, token listrik, dan merekrut 60 mitra pengemudi dalam sebulan. Kini, bisnis yang saya lakukan melalui platform ini telah memberikan kontribusi lebih dari 30 persen dari total penghasilan saya.” ungkap Sumiati.

“Bermitra dengan Grab merupakan langkah PayTren dalam menunjukkan eksistensinya sebagai salah satu pelaku usaha dunia _Financial Technology_ dan pemberdayaan UMKM yang memang sudah menjadi _core business_ PayTren dalam pemberdayaan umat, kami memiliki visi yang sama akan masa depan Indonesia. 

Jangkauan kemitraan yang dimiliki oleh PayTren sudah _nationwide_, kami ingin memberdayakan kalangan menengah Indonesia dan membantu mereka memperoleh penghasilan tambahan dengan memanfaatkan ekonomi digital. Sebagai langkah awal, mitra-mitra PayTren dapat memperoleh penghasilan dari pendaftaran para pengemudi ke platform Grab. 

Kami sangat antusias untuk menjajaki cara-cara alternatif agar manfaat ekonomi digital dapat dirasakan dalam kehidupan sehari-hari masyarakat Indonesia.” papar Ustadz Yusuf Mansur, Founder & Owner Paytren.

Saat ini PayTren memiliki jaringan mitra yang tersebar di seluruh Indonesia, yang menarik berbagai kalangan mulai dari petani, ibu rumah tangga, pelaku UKM hingga pegawai kantoran. 

Para mitra mendaftarkan diri untuk memanfaatkan sejumlah kesempatan memperoleh penghasilan, seperti menjual paket internet dan pulsa telepon, token listrik, dan tiket pesawat dan kini rekrutmen mitra pengemudi Grab. 

Segera setelah menandatangani perjanjian kerja sama ini, para mitra PayTren akan diberikan pelatihan untuk mendaftarkan mitra pengemudi Grab yang baru. Perekrutan mitra pengemudi Grab dapat dilakukan melalui mulai pertengahan Januari 2018 dan akan menjadi fitur tambahan dalam aplikasi PayTren.

“Melalui kemitraan dengan PayTren, Grab menjalankan komitmen yang sama untuk membawa peluang ekonomi digital kepada kelas ekonomi menengah baik di daerah perkotaan maupun pedesaan di Tanah Air. Kini, kami membantu masyarakat untuk bergabung sebagai mitra pengemudi di lebih dari 100 kota dan agen di 500 kota. 

Dengan menambahkan kekuatan dan kehadiran PayTren di daerah-daerah pedesaan Indonesia, kami yakin dapat mempercepat ekspansi Grab di seluruh Indonesia,” papar Ongki Kurniawan, Managing Director, GrabPay Indonesia.

Pada awal 2017, Grab mengumumkan investasi sebesar US$700 juta melalui master plan ‘Grab 4 Indonesia’ 2020 untuk mendukung target Indonesia menjadi negara ekonomi digital terbesar di Asia Tenggara pada 2020. 

Pada Tahap Pertama rencana ini, Grab telah membuka pusat R&D di Kebayoran Baru, Jakarta dan berkomitmen untuk melakukan investasi di _startup-startup_ lokal yang bergerak di bidang pendanaan, termasuk integrasi dengan Kudo, _startup_ O2O lokal terkemuka. 

Pada bulan Mei, Grab telah meluncurkan Tahap Kedua dari master plan ‘Grab 4 Indonesia’ dan berkomitmen untuk membantu 5 juta pengusaha mikro memperoleh penghasilan dari sektor ekonomi digital Indonesia pada akhir 2018. Kemitraan dengan PayTren akan membawa Grab melangkah lebih maju dalam memberdayakan 3 juta pengusaha mikro pada awal 2018.

**###**

 **Tentang PayTren**

PayTren yang berkantor pusat di Bandung Jawa Barat ini merupakan perusahaan yang di pimpin oleh Ustad Yusuf Mansur dengan nama PT Veritra Sentosa Internasional (_TRENI_). Fokus perusahaan yaitu pada bidang _financial tehnology_dengan kekuatan jaringan komunitas dengan lebih dari 1,700.000 mitra yang tersebar diseluruh wilayah Indonesia dan juga mancanegara.

PayTren menjadi perusahaan penyedia layanan teknologi perantara transaksi terbaik di tingkat nasional melalui pemberdayaan manusia potensial dan mandiri dengan konsep jejaring yang _up to date_ sesuai perkembangan zaman dan kebutuhan masyarakat.

Secara misi, PayTren mendorong masyarakat pengguna _gadget_, khususnya _smartphone_ berbasis IOS dan Android, untuk meningkatkan fungsi dari hanya sekedar alat berkomunikasi menjadi alat untuk bertransaksi dengan manfaat dan keuntungan yang tidak akan didapatkan dari cara bertransaksi yang biasa.

Paytren meluncurkan produk yang sejalan dengan program pemerintah untuk membentuk masyarakat tanpa uang tunai (_cashless society_) secara nasional ataupun internasional.

 **Tentang Grab**

Grab merupakan layanan transportasi on-demand dan pembayaran mobile terdepan di Asia Tenggara. Kami menjawab sejumlah tantangan transportasi yang krusial dan mewujudkan kebebasan transportasi bagi 620 juta orang di Asia Tenggara. 

Produk utama Grab mencakup solusi berkendara bagi pengemudi maupun penumpang yang menekankan pada kenyamanan, keselamatan dan kepastian, termasuk platform pembayaran mobile, GrabPay, yang meningkatkan akses terhadap solusi pembayaran mobile bagi jutaan mitra pengemudi dan penumpang di seluruh Asia Tenggara dan memperdalam inklusi keuangan di wilayah tersebut. 

Di Indonesia, jaringan agen Grab membantu masyarakat yang tidak memiliki dan memiliki akses terbatas terhadap layanan perbankan untuk memanfaatkan platform GrabPay untuk ambil bagian dalam revolusi digital. Saat ini Grab memberikan layanan di Singapura, Indonesia, Filipina, Malaysia, Thailand, Vietnam dan Myanmar. Untuk informasi lebih lanjut, kunjungi laman kami di [http://www.grab.com.](http://www.grab.com. "http://www.grab.com.")

sumber: [https://www.grab.com/id/press/business/grab-dan-paytren-jalin-kerja-sama-strategis/](https://www.grab.com/id/press/business/grab-dan-paytren-jalin-kerja-sama-strategis/ "https://www.grab.com/id/press/business/grab-dan-paytren-jalin-kerja-sama-strategis/")