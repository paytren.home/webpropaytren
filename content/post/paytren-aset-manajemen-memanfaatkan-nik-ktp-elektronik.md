+++
categories = ["news"]
date = "2018-03-30T10:55:14+07:00"
description = "Ditandatangani perjanjian kerjasama antara PT. PayTren Aset Manajemen dengan Dirjen Kependudukan dan Pencatatan Sipil. "
featured = []
thumbnail = "/images/paytren-aset-managemen.png"
slug = ""
tags = ["paytren"]
title = "PayTren Aset Manajemen Memanfaatkan NIK KTP Elektronik"

+++
Ditandatangani perjanjian kerjasama antara PT. PayTren Aset Manajemen dengan Dirjen Kependudukan dan Pencatatan Sipil. Untuk akses data dan pemanfaatan Nomor Induk Kependudukan (NIK) KTP elektronik.

Dengan akses ini, diharapkan ada kemudahan dan simplikasi, bagi peminat reksadana “recehan” yang dikeluarkan oleh PayTren Aset Manajemen (PAM).

Seratusan ribu peminat reksadana PAM, kesulitan registrasi dan eksekusi. Sebab sebelumnya harus mengisi 72 form isian. Alhamdulillaah, dengan ditandatangani perjanjian kerjasama ini form isian yang perlu diisi tinggal sekitar 14 isian saja. Bahkan kelak lebih simpel lagi. Asal ada NIK dan cocok, peminat Reksadana bisa registrasi dan eksekusi.

Perjanjian kerjasama dihadiri langsung oleh Dirjen Dukcapil, Prof. Zudan Arif Fakrulloh, dkk, Kemendagri, Bu Kiki dkk, dari PT Kustodian Sentral Efek Indonesia (KSEI) dihadiri oleh Direktur Utama Friderica Widyasari Dewi, dan para pimpinan PayTren serta Daarul Qur’an.

Alhamdulillaah.