+++
categories = ["news"]
date = "2018-03-30T08:37:28+07:00"
description = "PayTren kembali melakukan inovasi bisnis dengan menyelenggarakan kerjasama strategis bersama dua mobile payment terkemuka, yakni Grab dan Kudo."
featured = []
thumbnail = "/images/WhatsApp-Image.jpeg"
slug = "peresmian-kerja-sama-strategis-paytren-grab-dan-kudo"
tags = ["paytren"]
title = "Peresmian Kerjasama Strategis PayTren, Grab, dan Kudo"

+++
PayTren kembali melakukan inovasi bisnis dengan menyelenggarakan kerjasama strategis bersama dua mobile payment terkemuka, yakni Grab dan Kudo. Kerjasama yang disebut sebagai kesepakatan atas kepercayaan antara ketiga belah pihak ini ditandai dengan penandatangan MOU bertajuk “Kerjasama Strategis antara PayTren, Grab & Kudo”. 

Acara tersebut dihadiri oleh masing-masing Founder dan Direksi.  Dari PayTren hadir Ustadz Yusuf Mansur selaku Founder dan Owner PayTren serta Hari Prabowo, SE. selaku CEO PayTren. Sementara dari Grab hadir Jason Thompson sebagai Managing Director GrabPay, Ongki Kurniawan Managing Director GrabPay Indonesia dan Ridzki Kramadibrarta selaku Managing Director Grab Indonesia. 

Sementara dari Kudo, diwakili oleh Albert Lucius selaku CEO & Co-Founder Kudo dan Agung Nugroho selaku CEO & Co-Founder Kudo. Para mitra  PayTren yang kerap disebut PayTreners juga hadir meramainkan acara ini bergabung dengan komunitas pengemudi Grab sehingga acara menjadi semakin semarak.

[![](https://news.treni.co.id/wp-content/uploads/2017/12/WhatsApp-Image-2017-12-13-at-13.54.12-300x225.jpeg =300x225)](https://news.treni.co.id/wp-content/uploads/2017/12/WhatsApp-Image-2017-12-13-at-13.54.12.jpeg)

Kerjasama Strategis ini diharapkan akan membangun sinergis dan ekspansi bisnis ke segmen yang lebih luas lagi antara ketiganya. Grab dan PayTren akan membentuk jaringan pengusaha mikro terbesar di Indonesia dengan lebih dari 3 juta anggota. 

Mitra-mitra PayTren juga akan memperoleh manfaat dari beragam cara baru untuk memperoleh penghasilan, dimulai dari komisi perekrutan mitra pengemudi Grab. Sinergi kemitraan antara ketiga mobile payment ini diharapkan mampu  untuk mempercepat ekspansi bisnisnya ke arah yang lebih luas lagi.

Bagi [Kudo](https://id.techinasia.com/tag/paytren) sebagai anak perusahaan Grab ini,  bertujuan untuk memperluas ekosistem layanan di kedua belah pihak, serta menyediakan akses kesejahteraan ekonomi digital yang lebih luas di masa mendatang. 

Salah satu bentuk konkret dari perjanjian kerja sama ini antara lain kehadiran layanan Kudo dalam aplikasi PayTren. Selain itu, mitra PayTren ke depannya juga bisa mendaftarkan pengemudi Grab lewat fitur perekrutan yang akan tersedia dalam aplikasi PayTren, paling lambat pertengahan Januari 2018.

Penandatangan kerja sama ini disambut baik oleh _founder_ dan _owner_ PayTren, Yusuf Mansur, karena menurutnya bisa menjadi kesempatan untuk mempelajari teknologi yang dimiliki Kudo dan Grab. Senada dengan _founder_ PayTren, Ongki Kurniawan selaku Managing Director GrabPay Indonesia mengaku optimis pihaknya bisa menggapai ekosistem yang lebih luas lagi dengan bermitra bersama PayTren. 

Dengan kehadiran PayTren di daerah-daerah pedesaan (rural) Indonesia, kami yakin dapat mempercepat ekspansi Grab di seluruh Indonesia.  Bagi Ongki Kurniawan, Managing Director GrabPay Indonesia.  

Meski Paytren tergolong baru, namun mobile payment ini memiliki reputasi internasional yang baik dan telah melebarkan sayapnya ke lebih dari 35 negara di dunia dan sebelumnya juga sempat beberapa kali melakukan kerja sama dengan beberapa pihak antara lain [Alcatel-Lucent Enterprise (ALE)](https://swa.co.id/swa/trends/alcatel-lucent-jalin-kerja-sama-dengan-paytren), [DOKU](https://inet.detik.com/business/d-3209131/doku-tembus-sejuta-pelanggan-e-money), dan [DIMO Pay](http://www.tribunnews.com/bisnis/2017/04/05/gandeng-dimo-pay-indonesia-paytren-luncurkan-teknologi-pembayaran-nontunai) dan lainnya.

Dalam  ada pertanyaan untuk mengejar target UKM binaan Grab, apakah proses kerja sama antara Grab dengan PayTren ini melibatkan perubahan opsi kepemilikian saham atau tidak, Jason Thompson selaku Head of GrabPay Global

menegaskan bahwa penandatanganan ini murni sebagai salah satu bagian dari proses untuk memenuhi komitmen investasi mereka terhadap UKM Indonesia yang telah diumumkan sejak awal 2017. 

Investasi yang dilakukan Grab ini bertujuan untuk mendukung perkembangannya di dalam negeri. Tahap kerja sama sebelumnya juga telah berjalan melalui peresmian pusat penelitian dan pengembangan (R&D) Grab di Jakarta, yang kemudian dilanjutkan dengan [akuisisi Grab terhadap Kudo di bulan April 2017.](https://id.techinasia.com/grab-resmi-akuisisi-kudo-demi-dukung-perkembangan-grabpay)

Baik Grab dan PayTren yang sama-sama sempat terkendala oleh perizinan lisensi uang elektronik (e-money) yang diterbitkan oleh Bank Indonesia.  Imbas dari hambatan ini, PayTren juga menonaktifkan untuk sementara semua transaksi kecuali registrasi mitra baru, demikian pula

Grab juga menonaktifkan fitur isi ulang saldo dari layanan pembayaran GrabPay Credits sejak 6 Oktober 2017 lalu hingga saat yang masih belum bisa ditentukan. 

Menanggapi soal hambatan lisensi e-money dari kedua belah pihak, Ustadz Yusuf Mansur mengaku optimis dan mewanti-wanti konsumen PayTren untuk menunggu kejutan berikutnya di tahun 2018.

Hal yang menarik lainnya yang perlu diapresiasi bagi perkembangan ekonomi  Indonesia adalah statement dari CEO PayTren, Hari Prabowo, SE, yang menegaskan bahwa PayTren, Grab dan Kudo sudah berkembang dengan caranya sendiri-sendiri bersama keunikannya masing-masing.  

Kerjasama Strategis ini akan membentuk konstelasi **“ekonomi inklusif”** yang lebih luas lagi karena ketiganya sudah menyediakan kelengkapan bisnisnya secara mandiri dan inovatif dalam mengembangkan pola bisnisnya mulai dari konsep digitalnya, penyediaan  barang, jasa dan feature bisnisnya yang secara tidak langsung telah  membangun financial technology yang konkrit bagi perusahaan dan para mitranya.

Grab, sebagai platform penyedia layanan transportasi _on-demand_ dan pembayaran _mobile_ terdepan di Asia Tenggara, hari ini mengumumkan kerja sama strategis dengan PayTren, salah satu aplikasi pembayaran dan transaksi _mobile_ terkemuka di Indonesia. 

 Dengan kerja sama antara ketiga belah pihak, akan memungkinkan PayTren dan Grab untuk mengintegrasikan kekuatan mitra pengemudi dan mitra yang dimilikinya, untuk membentuk jaringan pengusaha mikro terbesar di Indonesia dengan lebih dari tiga juta pengusaha mikro yang akan memperoleh penghasilan tambahan dan membagikan kesempatan kerja sebagai mitra pengemudi Grab kepada masyarakat di sekitarnya.

Kerja sama strategis antara Grab dan PayTren akan meningkatkan kapasitas para pengusaha mikro untuk menggerakkan usaha-usaha baru, yang pada gilirannya akan mendorong pembangunan ekonomi yang berbasis partisipasi masyarakat luas sebagai pelaku usaha dan percepatan persebaran lapangan usaha digital di Tanah Air. 

Mulai Januari 2018 mendatang, para mitra PayTren di seluruh Indonesia dapat merekrut mitra pengemudi Grab melalui aplikasi PayTren dan memperoleh pendapatan tambahan. Ke depannya, melalui teknologi dan produk yang disediakan oleh Kudo yang dimiliki Grab, mitra PayTren akan semakin memiliki beragam potensi usaha menarik yang dapat meningkatkan kesejahteraan mereka.

Ekonomi Digital Indonesia terus berkembang pesat dengan tingginya tingkat adopsi internet, namun karena rendahnya tingkat adopsi rekening bank, banyak kalangan menengah di Indonesia yang belum dapat menikmati manfaat dari ekonomi digital. 

Model bisnis yang dimiliki oleh PayTren dan platform teknologi Kudo yang dimiliki Grab telah terbukti sebagai solusi lokal yang sukses mengatasi tantangan tersebut. Para agen tersebut akan membantu masyarakat Indonesia yang memiliki pengalaman terbatas atau bahkan mereka yang tidak memiliki pengalaman digital sama sekali untuk mengakses layanan _online_.

Rikrik Saptaria